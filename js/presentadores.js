import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import { getAuth } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-storage.js';
import { getDatabase, ref as databaseRef, onValue } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-database.js';

let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const storage = getStorage(app);
const database = getDatabase(app);

function deletePr(name) {
    let data = {
        apellidoMaterno: null,
        apellidoPaterno: null,
        carrera: null,
        celular: null,
        matricula: null,
        nombre: null

    };

    console.log(JSON.stringify(data, null, 2));

    fetch(`${url}/presentadores/${name}.json`, {
        method: 'PATCH',
        body: JSON.stringify(data, null, 2),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
        .then(response => response.json())
        .catch(error => {
            console.error("Ha ocurrido un error: ", error);
            alert("Ha ocurrido un error al eliminar el presentador.");
        });
    displayAndRenderConferencias();
}

async function displayAndRenderConferencias() {
    try {
        const response = await fetch(`${url}/presentadores.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const presentadores = await response.json();
        console.log("Datos consultados:", presentadores);

        const table = document.createElement("table");
        const thead = document.createElement("thead");
        const tbody = document.createElement("tbody");
        table.appendChild(thead);
        table.appendChild(tbody);
        table.classList.add('table');
        table.classList.add('table-dark');
        document.getElementById('productList').innerHTML = '';
        document.getElementById('productList').appendChild(table);

        // Crear encabezado de la tabla
        const trHeader = document.createElement("tr");

        ["Nombre", "Apellido Paterno", "Apellido Materno", "Carrera", "Telefono", "Matricula", "Acciones"].forEach(headerText => {
            const th = document.createElement("th");
            th.textContent = headerText;
            trHeader.appendChild(th);
        });
        thead.appendChild(trHeader);

        if (presentadores) {
            for (const presentadoresKey in presentadores) {
                if (Object.hasOwnProperty.call(presentadores, presentadoresKey)) {
                    const presentador = presentadores[presentadoresKey];
                    console.log("presentador consultado: " + presentadoresKey);


                    const tr = document.createElement("tr");
                    tr.id = `product-${presentador}`;  // Aseguramos un ID único

                    // Nombre
                    const tdNombre = document.createElement("td");
                    tdNombre.textContent = presentador.nombre;
                    tr.appendChild(tdNombre);

                    // Apellido Paterno
                    const tdApePa = document.createElement("td");
                    tdApePa.textContent = presentador.apellidoPaterno;
                    tr.appendChild(tdApePa);

                    // Apellido Materno
                    const tdApeMa = document.createElement("td");
                    tdApeMa.textContent = presentador.apellidoMaterno;
                    tr.appendChild(tdApeMa);

                    //Carrera
                    const carrera = document.createElement("td");
                    const idCarrera = presentador.carrera;


                    try {
                        console.log("Consultando datos para UID:", idCarrera);
                        const response = await fetch(`${url}/carreras/${idCarrera}.json`);

                        if (!response.ok) {
                            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
                        }

                        const users = await response.json();
                        console.log("Datos consultados carrera:", users);

                        if (users) {
                            carrera.textContent = users;
                            tr.appendChild(carrera);
                        }
                    } catch (error) {
                        console.error("Ha ocurrido un error: ", error);
                    }

                    // Telefono
                    const telefono = document.createElement("td");
                    telefono.textContent = presentador.celular;
                    tr.appendChild(telefono);

                    // Matricula
                    const matricula = document.createElement("td");
                    matricula.textContent = presentador.matricula;
                    tr.appendChild(matricula);

                    // Acciones
                    const tdActions = document.createElement("td");
                    const deleteButton = document.createElement("button");
                    deleteButton.classList.add('botonE');
                    deleteButton.innerText = "Eliminar";
                    deleteButton.onclick = () => deletePr(presentadoresKey);
                    tdActions.appendChild(deleteButton);
                    tr.appendChild(tdActions);

                    tbody.appendChild(tr);
                }
            }
        } else {
            console.log('No hay presentadores');
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }
}

guardar.addEventListener('click', (e) => {
    var name = document.getElementById('nombrePr').value;
    var pApe = document.getElementById('apellidoPaPr').value;
    var mApe = document.getElementById('apellidoMaPr').value;
    var cel = document.getElementById('tel').value;
    var matricula = document.getElementById('matricula').value;
    var carrera = document.getElementById('carreras').value;

    let user = {
        nombre: name,
        apellidoPaterno: pApe,
        apellidoMaterno: mApe,
        celular: cel,
        matricula: matricula,
        carrera: carrera,
    }

    console.log(JSON.stringify(user, null, 2));
    fetch(`${url}/presentadores/${name}.json`, {
        method: 'PUT',
        body: JSON.stringify(user, null, 2),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
        .then(response => response.json())
        .catch(error => console.error("Ha ocurrido un error: ", error));

    alert("Presentador creado");
    displayAndRenderConferencias();
});

document.addEventListener("DOMContentLoaded", function () {
    displayAndRenderConferencias();
});

const conferencesRef = databaseRef(database, 'conferencias');

onValue(conferencesRef, (snapshot) => {
    displayAndRenderConferencias();
});