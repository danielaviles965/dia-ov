import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import { getAuth } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-storage.js';
import { getDatabase, ref as databaseRef, onValue } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-database.js';

let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const storage = getStorage(app);
const database = getDatabase(app);

function deleteAs(name) {
    let data = {
        estado: 0
    };

    console.log(JSON.stringify(data, null, 2));

    fetch(`${url}/users/${name}.json`, {
        method: 'PATCH',
        body: JSON.stringify(data, null, 2),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
        .then(response => response.json())
        .catch(error => {
            console.error("Ha ocurrido un error: ", error);
            alert("Ha ocurrido un error al eliminar el aspirante.");
        });
}

async function displayAndRenderConferencias() {
    try {
        const response = await fetch(`${url}/users.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const aspirantes = await response.json();
        console.log("Datos consultados:", aspirantes);

        const table = document.createElement("table");
        const thead = document.createElement("thead");
        const tbody = document.createElement("tbody");
        table.appendChild(thead);
        table.appendChild(tbody);
        table.classList.add('table');
        table.classList.add('table-dark');
        document.getElementById('productList').innerHTML = '';
        document.getElementById('productList').appendChild(table);

        // Crear encabezado de la tabla
        const trHeader = document.createElement("tr");

        ["Nombre", "Apellido Paterno", "Apellido Materno", "Escuela de Procedencia", "Correo", "Acciones"].forEach(headerText => {
            const th = document.createElement("th");
            th.textContent = headerText;
            trHeader.appendChild(th);
        });
        thead.appendChild(trHeader);

        if (aspirantes) {
            for (const aspirantesKey in aspirantes) {
                if (Object.hasOwnProperty.call(aspirantes, aspirantesKey)) {
                    const aspirante = aspirantes[aspirantesKey];
                    console.log("aspirante consultado: "+aspirantesKey);

                    if (aspirante.privilegio === 0 && aspirante.estado === 1) {
                        const tr = document.createElement("tr");
                        tr.id = `product-${aspirante}`;  // Aseguramos un ID único

                        // Nombre
                        const tdNombre = document.createElement("td");
                        tdNombre.textContent = aspirante.nombre;
                        tr.appendChild(tdNombre);

                        // Apellido Paterno
                        const tdApePa = document.createElement("td");
                        tdApePa.textContent = aspirante.apellidoPaterno;
                        tr.appendChild(tdApePa);

                        // Apellido Materno
                        const tdApeMa = document.createElement("td");
                        tdApeMa.textContent = aspirante.apellidoMaterno;
                        tr.appendChild(tdApeMa);

                        // Escuela Procedncia
                        const escuela = document.createElement("td");
                        escuela.textContent = aspirante.escuelaProcedencia;
                        tr.appendChild(escuela);

                        // Correo
                        const correo = document.createElement("td");
                        correo.textContent = aspirante.correo;
                        tr.appendChild(correo);

                        // Acciones
                        const tdActions = document.createElement("td");
                        const deleteButton = document.createElement("button");
                        deleteButton.classList.add('botonE');
                        deleteButton.innerText = "Eliminar";
                        deleteButton.onclick = () => deleteAs(aspirantesKey);
                        tdActions.appendChild(deleteButton);
                        tr.appendChild(tdActions);

                        tbody.appendChild(tr);
                    }
                }
            }
        } else {
            console.log('No hay aspirantes');
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
        alert("Ha ocurrido un error al mostrar aspirantes.");
    }
}

displayAndRenderConferencias();

document.addEventListener("DOMContentLoaded", function () {
    displayAndRenderConferencias();
});

const conferencesRef = databaseRef(database, 'conferencias');

onValue(conferencesRef, (snapshot) => {
    displayAndRenderConferencias();
});