import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import {
    sendEmailVerification, getAuth, signInWithPopup,
    createUserWithEmailAndPassword, signInWithEmailAndPassword,
    onAuthStateChanged
} from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';

let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

let uid = "";

onAuthStateChanged(auth, (user) => {
    if (user) {
        uid = user.uid;
        console.log("UID del usuario:", uid);

        // Llama a la función Carreras después de configurar todo
        Carreras();
    } else {
        console.log("El usuario no está autenticado.");
    }
});

async function Carreras() {
    try {
        console.log("Consultando datos para UID:", uid);
        const response = await fetch(`${url}/users/${uid}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const users = await response.json();
        console.log("Datos consultados:", users);

        if (users) {
            if (users.c1 === 0 && users.c2 === 0 && users.c3 === 0 && users.c4 === 0 && users.c5 === 0) {
                // ConfirmarButton está ahora disponible en este ámbito
                confirmarButton.addEventListener('click', (e) => {
                    confirmarSeleccion(uid);
                });
            } else {
                mostrarResultado(uid);
            }
        } else {
            console.log("No hay datos asociados al UID en la base de datos.");
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }
}

function confirmarSeleccion(uid) {
    // Obtén los elementos select
    var selects = [
        document.getElementById("carrera1"),
        document.getElementById("carrera2"),
        document.getElementById("carrera3"),
        document.getElementById("carrera4"),
        document.getElementById("carrera5")
    ];

    // Almacena las opciones seleccionadas en un formato adecuado para la base de datos
    var carrerasSeleccionadas = [];

    // Verifica que se hayan seleccionado exactamente 5 carreras
    if (selects.every(select => select.value !== "0")) {
        // Recorre cada select y almacena la carrera seleccionada
        selects.forEach((select, index) => {
            var carreraSeleccionada = select.value;
            carrerasSeleccionadas.push({ [`c${index + 1}`]: carreraSeleccionada });
        });

        // Actualiza los datos en la base de datos
        var aggC = {};
        carrerasSeleccionadas.forEach(carrera => {
            Object.assign(aggC, carrera);
        });

        // Realiza la solicitud HTTP para actualizar los datos en la base de datos
        fetch(`${url}/users/${uid}.json`, {
            method: 'PATCH',
            body: JSON.stringify(aggC),
            headers: { 'Content-type': 'application/json; charset=UTF-8' }
        })
        .then(response => {
            if (!response.ok) {
                throw new Error(`Error al actualizar datos: ${response.status} ${response.statusText}`);
            }
            return response.json();
        })
        .then(data => {
            console.log("Datos actualizados:", data);
            mostrarResultado(uid);
        })
        .catch(error => console.error("Ha ocurrido un error: ", error));
    } else {
        alert("Debes seleccionar exactamente 5 carreras.");
    }
}

async function mostrarResultado(uid) {
    const carrerasInfoURL = `${url}/carreras.json`; // URL de las carreras
    const carrerasInfoResponse = await fetch(carrerasInfoURL);

    if (!carrerasInfoResponse.ok) {
        console.error(`Error al obtener la información de carreras: ${carrerasInfoResponse.status} ${carrerasInfoResponse.statusText}`);
        return;
    }

    const carrerasInfo = await carrerasInfoResponse.json();
    console.log("Consultando datos para carreras:", carrerasInfo);

    const cuerpoTabla = document.getElementById("cuerpoTabla");

    // Limpiar el cuerpo de la tabla
    cuerpoTabla.innerHTML = "";

    try {
        console.log("Consultando datos para UID:", uid);
        const response = await fetch(`${url}/users/${uid}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const userData = await response.json();
        console.log("Datos consultados:", userData);

        if (userData) {
            // Crear un array con las carreras seleccionadas
            const carrerasSeleccionadas = [
                userData.c1, userData.c2, userData.c3, userData.c4, userData.c5
            ].filter(numero => numero !== 0);

            // Obtener los nombres de las carreras usando la información de carreras
            const carrerasNombres = carrerasSeleccionadas.map(numero => carrerasInfo[numero]);

            // Llenar la tabla con las carreras seleccionadas en el orden del formulario
            carrerasNombres.forEach(function (nombreCarrera, index) {
                var fila = cuerpoTabla.insertRow();
                var celda = fila.insertCell(0);
                celda.textContent = `${index + 1}.  ${nombreCarrera}`;
            });
        } else {
            console.log("No hay datos asociados al UID en la base de datos.");
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }

    // Ocultar el formulario
    document.getElementById("formularioCarreras").style.display = "none";
    document.getElementById("titulo").style.display = "none";
    document.getElementById("sub").style.display = "block";
    // Mostrar el resultado
    document.getElementById("resultado").style.display = "block";
}



document.addEventListener("DOMContentLoaded", function () {
    // Obtén los elementos select
    var selects = [
        document.getElementById("carrera1"),
        document.getElementById("carrera2"),
        document.getElementById("carrera3"),
        document.getElementById("carrera4"),
        document.getElementById("carrera5")
    ];

    // Botón de confirmar
    var confirmarButton = document.getElementById("confirmar");

    // Almacena las opciones seleccionadas para evitar duplicados
    var opcionesSeleccionadas = [];

    // Función para deshabilitar opciones seleccionadas en los demás selects
    function deshabilitarOpcionesSeleccionadas() {
        selects.forEach((select, index) => {
            Array.from(select.options).forEach(option => {
                if (opcionesSeleccionadas.includes(option.value) && option.value !== "0") {
                    option.disabled = true;
                }
            });
        });
    }

    // Agrega un evento de cambio a cada select
    selects.forEach((select, index) => {
        select.addEventListener("change", function () {
            var opcionSeleccionada = select.value;

            // Habilita todas las opciones en todos los selects
            selects.forEach(s => {
                Array.from(s.options).forEach(option => {
                    option.disabled = false;
                });
            });

            // Agrega la opción seleccionada a la lista de seleccionadas
            opcionesSeleccionadas[index] = opcionSeleccionada;

            // Deshabilita opciones seleccionadas en los demás selects
            deshabilitarOpcionesSeleccionadas();

            // Verifica si todas las selecciones son diferentes de 0 (ninguna carrera seleccionada)
            var todasSeleccionadas = opcionesSeleccionadas.every(opcion => opcion !== "0");

            // Habilita o deshabilita el botón según el resultado
            confirmarButton.disabled = !todasSeleccionadas;
        });
    });

    // Agrega el evento click al botón Confirmar
    confirmarButton.addEventListener('click', function () {
        confirmarSeleccion(uid);
    });
});
