import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import {
    sendEmailVerification, getAuth, signInWithPopup,
    createUserWithEmailAndPassword, signInWithEmailAndPassword,
    onAuthStateChanged
} from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

let uid = "";


onAuthStateChanged(auth, (user) => {
    if (user) {
        uid = user.uid;
        console.log("UID del usuario:", uid);
        NameConsult();
    } else {
        console.log("El usuario no está autenticado.");
    }
});

async function NameConsult() {
    try {
        console.log("Consultando datos para UID:", uid);
        const response = await fetch(`${url}/users/${uid}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const users = await response.json();
        console.log("Datos consultados:", users);

        if (users) {
            const userBienElement = document.getElementById('userBien');
            if (userBienElement) {
                userBienElement.textContent = users.nombre;
                console.log("Texto asignado correctamente al elemento HTML.");
            } else {
                console.log("Elemento HTML 'userBien' no encontrado.");
            }
        } else {
            console.log("No hay datos asociados al UID en la base de datos.");
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }
}

cerrarS.addEventListener('click', (e) =>{
    auth.signOut().then(() => {
        alert('Sesion cerrada exitosamente');
    }).catch((error) => {
        alert("Error! al cerrar la sesion");
    })
});

cerrar.addEventListener('click', (e) =>{
    auth.signOut().then(() => {
        alert('Sesion cerrada exitosamente');
    }).catch((error) => {
        alert("Error! al cerrar la sesion");
    })
});
