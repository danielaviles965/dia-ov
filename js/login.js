import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import {
    sendEmailVerification, getAuth, signInWithPopup,
    createUserWithEmailAndPassword, signInWithEmailAndPassword,
    onAuthStateChanged
} from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

let url = "https://dia-ov-default-rtdb.firebaseio.com/";

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

//Cerrar sesion
auth.signOut();

login.addEventListener('click', (e) => {
    var email = document.getElementById('emaillog').value;
    var password = document.getElementById('passwordlog').value;

    signInWithEmailAndPassword(auth, email, password).then(cred => {

    }).catch(error => {
        const errorCodigo = error.code;

        if (errorCodigo == 'auth/user-disabled')
            alert('el usuario ah sido deshabilitado');
        else if (errorCodigo == 'auth/invalid-email')
            alert('el correo no es valido');
        else if (errorCodigo == 'auth/user-not-found')
            alert('el usuario no existe');
        else if (errorCodigo == 'auth/wrong-password')
            alert('contraseña incorrecta');
    });
    resetInput();
});

let uid = "";

auth.onAuthStateChanged(user => {
    if (user) {
        console.log("usuario activo");
        var email = user.emailVerified;
        if (email) {
            uid = user.uid;
            console.log("Consultando datos para UID:", uid);
            verUser();
        } else {
            alert("Correo no verificado aun");
            auth.signOut();
        }
    } else {
        console.log('usuario inactivo');
        auth.signOut();
    }
});

async function verUser(){
    try {
        console.log("Consultando datos para UID:", uid);
        const response = await fetch(`${url}/users/${uid}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const users = await response.json();
        console.log("Datos consultados:", users);

        if (users) {
            const permisos = users.privilegio;
            console.log("Permisos: "+permisos);
            if (permisos === 0) {
                if (users.estado === 0) {
                    alert("Usuario deshabilitado");
                    resetInput();
                    auth.signOut();
                } else{
                    alert("Usuario logeado");
                    location.href = '/html/menu.html';
                }
            } else {
                if (users.estado === 0) {
                    alert("Usuario deshabilitado");
                    resetInput();
                    auth.signOut();
                } else{
                    alert("Usuario logeado");
                    location.href = '/html/menuAdmin.html';
                }
            }
        } else {
            console.log("No hay datos asociados al UID en la base de datos.");
            auth.signOut();
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
        auth.signOut();
    }
}

function resetInput(){
    document.getElementById('emaillog').value = '';
    document.getElementById('passwordlog').value = '';
}
