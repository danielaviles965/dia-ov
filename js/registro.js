import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import {sendEmailVerification, getAuth, signInWithPopup, 
    createUserWithEmailAndPassword, signInWithEmailAndPassword,  
    onAuthStateChanged} from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

registro.addEventListener('click', (e) => { 
    var name = document.getElementById('txtNombre').value;
    var pApe = document.getElementById('txtPrimerApellido').value;
    var mApe = document.getElementById('txtSegundoApellido').value;
    var password = document.getElementById('passwordreg').value;
    var organismoSelect = document.getElementById('organismo');
    var escuela = organismoSelect.options[organismoSelect.selectedIndex].text;
    var email = document.getElementById('emailreg').value;
    const estado = 1;
    const permisos = 0; //0 es aspirante y 1 es administrador

    


    createUserWithEmailAndPassword(auth, email, password).then(cred =>{
        alert("Usuario creado");
        sendEmailVerification(auth.currentUser).then(()=>{
            let user= {
               nombre : name,
               apellidoPaterno: pApe,
               apellidoMaterno: mApe,
               escuelaProcedencia: escuela,
               correo: email,
               estado: estado,
               privilegio: permisos,
               c1: 0,
               c2: 0,
               c3: 0,
               c4: 0,
               c5: 0
            }

            console.log(JSON.stringify(user, null, 2));
            fetch(`${url}/users/${cred.user.uid}.json`, {
                method: 'PUT',
                body: JSON.stringify(user, null, 2),
                headers: { 'Content-type': 'application/json; charset=UTF-8' }
            })
                .then(response => response.json())
                .catch(error => console.error("Ha ocurrido un error: ", error));
            
                alert('se ha enviado un correo de verificacion');
            location.href = '/html/login.html';
        });
    }).catch(error =>{
        const errorCodigo = error.code;

        if(errorCodigo == 'auth/email-already-in-use')
            alert('el correo ya esta en uso');
        else if(errorCodigo == 'auth/invalid-email')
            alert('el correo no es valido');
        else if(errorCodigo == 'auth/weak-password')
            alert('la contraseña debe tener al menos 6 caracteres');
    });
});
