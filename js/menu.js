import { initializeApp } from "https://www.gstatic.com/firebasejs/9.0.2/firebase-app.js";
import { getAuth } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-auth.js';
import { getStorage, ref as storageRef, uploadBytesResumable, getDownloadURL } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-storage.js';
import { getDatabase, ref as databaseRef, onValue } from 'https://www.gstatic.com/firebasejs/9.0.2/firebase-database.js';

let url = "https://dia-ov-default-rtdb.firebaseio.com/";

const firebaseConfig = {
    apiKey: "AIzaSyAurSOm_9sjElYE-M0gulZ0ajU4KnlCz9U",
    authDomain: "dia-ov.firebaseapp.com",
    projectId: "dia-ov",
    storageBucket: "dia-ov.appspot.com",
    messagingSenderId: "384303458866",
    appId: "1:384303458866:web:5599872d59f5462e342a2b",
    measurementId: "G-3Q3DL31B1F"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app);
const storage = getStorage(app);
const database = getDatabase(app);

function uploadImage(file, callback) {
    const sRef = storageRef(storage, 'imagen/' + Date.now() + '-' + file.name);
    const uploadTask = uploadBytesResumable(sRef, file);

    uploadTask.on('state_changed',
        (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
        },
        (error) => {
            console.error('Error uploading image:', error);
        },
        () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                callback(downloadURL);
            });
        }
    );
}

function resetForm() {
    document.getElementById('nombre').value = '';
    document.getElementById('descripcion').value = '';
    document.getElementById('presentadorSec').value = '';
    document.getElementById('fechayhora').value = '';
    document.getElementById('aula').value = '';
    document.getElementById('imagenpresentador').value = '';
}

const presen = generarPresentadores();

document.getElementById('productForm').addEventListener('submit', function (event) {
    event.preventDefault();
    var name = document.getElementById('nombre').value;
    var descripcion = document.getElementById('descripcion').value;

    var presentaSelect = document.getElementById('presentadorSec');

    var presentador = presentaSelect.options[presentaSelect.selectedIndex].text;

    const fechayhora = document.getElementById('fechayhora').value;

    var aula = document.getElementById('aula').value;

    var imagen = document.getElementById('imagenpresentador').files[0];

    saveConferencia(name, descripcion, presentador, fechayhora, aula, imagen);
    resetForm();
});

async function generarPresentadores() {
    try {
        const response = await fetch(`${url}/presentadores.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const presentadores = await response.json();
        console.log("Datos consultados:", presentadores);

        if (presentadores) {
            for (const presentadoresKey in presentadores) {
                if (Object.hasOwnProperty.call(presentadores, presentadoresKey)) {
                    const presentador = presentadores[presentadoresKey];
                    console.log("presentador consultado: " + presentadoresKey);

                    const $presentador = presentador.nombre;
                    const sex = document.getElementById('presentadorSec');

                    const option = document.createElement('option');
                    const valor = $presentador;
                    option.id = valor;
                    option.value = valor;
                    option.text = valor;
                    sex.appendChild(option);
                }
            }
        } else {
            console.log('No hay presentadores');
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }
}

function saveConferencia(name, descripcion, presentador, fechayhora, aula, imagen) {
    if (!name || !descripcion || !presentador || !fechayhora || !aula || !imagen) {
        alert("Datos incompletos, no se guardará la conferencia");
        return;
    }

    console.log("Fecha a mandar:" + fechayhora)
    function storeData(imageUrl) {
        let data = {
            nombre: name,
            descripcion: descripcion,
            presentador: presentador,
            fechayhora: fechayhora,
            aula: aula,
            imageUrl: imageUrl
        };

        console.log(JSON.stringify(data, null, 2));

        fetch(`${url}/conferencias/${name}.json`, {
            method: 'PATCH',
            body: JSON.stringify(data, null, 2),
            headers: { 'Content-type': 'application/json; charset=UTF-8' }
        })
            .then(response => response.json())
            .catch(error => {
                console.error("Ha ocurrido un error: ", error);
                alert("Ha ocurrido un error al subir la imagen de la conferencia.");
            });
    }

    if (imagen) {
        uploadImage(imagen, (imageUrl) => {
            storeData(imageUrl);
        });
    } else {
        storeData(null);
    }
}

async function llenarDatos(name) {
    try {
        const response = await fetch(`${url}/conferencias/${name}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const conferencia = await response.json();
        console.log("Datos consultados:", conferencia);

        if (conferencia) {
            document.getElementById('nombre').value = conferencia.nombre;
            document.getElementById('descripcion').value = conferencia.descripcion;
            document.getElementById('presentadorSec').value = conferencia.presentador;
            document.getElementById('fechayhora').value = conferencia.fechayhora;
            document.getElementById('aula').value = conferencia.aula;
        } else {
            console.log("No hay datos asociados nombre en la base de datos.");
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
        alert("Ha ocurrido un error al llenar los datos de la conferencia.");
    }
}

function deleteCon(name) {
    let data = {
        nombre: null,
        descripcion: null,
        presentador: null,
        fechayhora: null,
        aula: null,
        imageUrl: null
    };

    console.log(JSON.stringify(data, null, 2));

    fetch(`${url}/conferencias/${name}.json`, {
        method: 'PATCH',
        body: JSON.stringify(data, null, 2),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
        .then(response => response.json())
        .catch(error => {
            console.error("Ha ocurrido un error: ", error);
            alert("Ha ocurrido un error al eliminar la conferencia.");
        });
}

async function AulaConsult(uid) {
    try {
        console.log("Consultando datos para UID:", uid);
        const response = await fetch(`${url}/aulas/${uid}.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const users = await response.json();
        console.log("Datos consultados aula:", users);

        if (users) {
            const aula = users;
            console.log("Aula consultada final: " + aula);
            return aula;
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
    }
}

async function displayAndRenderConferencias() {
    try {
        const response = await fetch(`${url}/conferencias.json`);

        if (!response.ok) {
            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
        }

        const conferencias = await response.json();
        console.log("Datos consultados:", conferencias);

        const table = document.createElement("table");
        const thead = document.createElement("thead");
        const tbody = document.createElement("tbody");
        table.appendChild(thead);
        table.appendChild(tbody);
        table.classList.add('table');
        table.classList.add('table-dark');
        document.getElementById('productList').innerHTML = '';
        document.getElementById('productList').appendChild(table);

        // Crear encabezado de la tabla
        const trHeader = document.createElement("tr");

        ["Titulo", "Descripción", "Presentador", "Fecha y Hora", "Aula", "Imagen", "Acciones", "CodigoQR"].forEach(headerText => {
            const th = document.createElement("th");
            th.textContent = headerText;
            trHeader.appendChild(th);
        });
        thead.appendChild(trHeader);

        if (conferencias) {
            for (const conferenciaKey in conferencias) {
                if (Object.hasOwnProperty.call(conferencias, conferenciaKey)) {
                    const conferencia = conferencias[conferenciaKey];

                    const tr = document.createElement("tr");
                    tr.id = `product-${conferencia.nombre}`;  // Aseguramos un ID único

                    // Nombre
                    const tdNombre = document.createElement("td");
                    tdNombre.textContent = conferencia.nombre;
                    tr.appendChild(tdNombre);

                    // Descripcion
                    const tdDescripcion = document.createElement("td");
                    tdDescripcion.textContent = conferencia.descripcion;
                    tr.appendChild(tdDescripcion);

                    // Presentador
                    const tdPresentador = document.createElement("td");
                    tdPresentador.textContent = conferencia.presentador;
                    tr.appendChild(tdPresentador);

                    // FechayHora
                    const valorInput = conferencia.fechayhora;
                    const fecha = new Date(valorInput);
                    const fechaFormateada = `${(fecha.getMonth() + 1).toString().padStart(2, '0')}/${fecha.getDate().toString().padStart(2, '0')}/${fecha.getFullYear()} ${fecha.getHours().toString().padStart(2, '0')}:${fecha.getMinutes().toString().padStart(2, '0')} ${fecha.getHours() >= 12 ? 'PM' : 'AM'}`;
                    // Imprime el valor formateado
                    console.log(fechaFormateada);
                    const tdFechayHora = document.createElement("td");
                    tdFechayHora.textContent = fechaFormateada;
                    tr.appendChild(tdFechayHora);

                    // Aula
                    const tdAula = document.createElement("td");
                    const aula = conferencia.aula;

                    try {
                        console.log("Consultando datos para UID:", aula);
                        const response = await fetch(`${url}/aulas/${aula}.json`);

                        if (!response.ok) {
                            throw new Error(`Error al consultar datos: ${response.status} ${response.statusText}`);
                        }

                        const users = await response.json();
                        console.log("Datos consultados aula:", users);

                        if (users) {
                            tdAula.textContent = users;
                            tr.appendChild(tdAula);
                        }
                    } catch (error) {
                        console.error("Ha ocurrido un error: ", error);
                    }

                    // Imagen
                    const tdImage = document.createElement("td");
                    const img = document.createElement("img");
                    img.src = conferencia.imageUrl;
                    img.alt = conferencia.nombre;
                    img.width = 100;
                    tdImage.appendChild(img);
                    tr.appendChild(tdImage);

                    // Acciones
                    const tdActions = document.createElement("td");
                    const deleteButton = document.createElement("button");
                    deleteButton.classList.add('botonE');
                    deleteButton.innerText = "Eliminar";
                    deleteButton.onclick = () => deleteCon(conferencia.nombre);
                    tdActions.appendChild(deleteButton);

                    const editButton = document.createElement("button");
                    editButton.classList.add('botonE');
                    editButton.innerText = "Editar";
                    editButton.onclick = () => llenarDatos(conferencia.nombre);
                    tdActions.appendChild(editButton);
                    tr.appendChild(tdActions);

                    tbody.appendChild(tr);

                    // CodigoQR
                    const tdQR = document.createElement("td");
                    const qr = document.createElement("div");
                    qr.classList.add('qr');
                    const QR = new QRCode(qr);
                    QR.makeCode("http://127.0.0.1:5501/html/#" + tr.id);

                    tdQR.appendChild(qr);
                    tr.appendChild(tdQR);

                    tbody.appendChild(tr);
                }
            }
        } else {
            console.log('No hay conferencias');
        }
    } catch (error) {
        console.error("Ha ocurrido un error: ", error);
        alert("Ha ocurrido un error al mostrar las conferencias.");
    }
}

document.addEventListener("DOMContentLoaded", function () {
    displayAndRenderConferencias();
});

const conferencesRef = databaseRef(database, 'conferencias');

onValue(conferencesRef, (snapshot) => {
    displayAndRenderConferencias();
});

cerrarS.addEventListener('click', (e) => {
    auth.signOut().then(() => {
        alert('Sesion cerrada exitosamente');
    }).catch((error) => {
        alert("Error! al cerrar la sesion");
    })
});

cerrar.addEventListener('click', (e) => {
    auth.signOut().then(() => {
        alert('Sesion cerrada exitosamente');
    }).catch((error) => {
        alert("Error! al cerrar la sesion");
    })
});


